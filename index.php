<?php 

require 'model/question.php';

$question = \Model\Question::retrieve(2);

echo $question->description, PHP_EOL;
echo "Options: ", PHP_EOL;

foreach($question->options as $option) {
  echo $option->value, PHP_EOL;
}

echo "Answer: ", PHP_EOL;
echo $question->answer;

?>
