INSERT INTO questions (description, answer)
VALUES (
  'What\'s the resource that you generally get first?',
  'wood'
);

INSERT INTO questions (description, answer)
VALUES (
  'How many growth stages does the wheat have?',
  '8'
);

INSERT INTO questions (description, answer)
VALUES (
  'What resources are needed to operate a combustion engine?',
  'water with oil or magma'
);

INSERT INTO questions (description, answer)
VALUES (
  'What is the name of the flower used to generate mana with coal?',
  'endoflame'
);

INSERT INTO questions (description, answer)
VALUES (
  'How many blocks of copper cable are necessary in order to it start losing energy?',
  '4'
);
