-- Options for question 1
INSERT INTO options (value, question_id) VALUES ('wood', 1);

INSERT INTO options (value, question_id) VALUES ('stone', 1);

INSERT INTO options (value, question_id) VALUES ('dirt', 1);

INSERT INTO options (value, question_id) VALUES ('coal', 1);

-- Options for question 2
INSERT INTO options (value, question_id) VALUES ('8', 2);

INSERT INTO options (value, question_id) VALUES ('5', 2);

INSERT INTO options (value, question_id) VALUES ('6', 2);

INSERT INTO options (value, question_id) VALUES ('7', 2);

-- Options for question 3
INSERT INTO options (value, question_id) VALUES ('water with oil or magma', 3);

INSERT INTO options (value, question_id) VALUES ('water with oil and magma', 3);

INSERT INTO options (value, question_id) VALUES ('water and oil only', 3);

INSERT INTO options (value, question_id) VALUES ('oil and magma only', 3);

-- Options for question 4
INSERT INTO options (value, question_id) VALUES ('endoflame', 4);

INSERT INTO options (value, question_id) VALUES ('exogleed', 4);

INSERT INTO options (value, question_id) VALUES ('endofire', 4);

INSERT INTO options (value, question_id) VALUES ('exoember', 4);

-- Options for question 5
INSERT INTO options (value, question_id) VALUES ('4', 5);

INSERT INTO options (value, question_id) VALUES ('5', 5);

INSERT INTO options (value, question_id) VALUES ('6', 5);

INSERT INTO options (value, question_id) VALUES ('7', 5);
