<?php 

namespace DB;

class Connection {
  private static $instance;
  private static $dbname = "mcquiz";

  public static function sharedInstance() {
    if(!isset(self::$instance)) {
      self::$instance = new \PDO("mysql:host=localhost;dbname=" . self::$dbname, "root", "root");
      self::$instance->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    return self::$instance;
  }
}

?>
