<?php 

namespace Auth;

class Service {
  private $conn;

  public function __construct() {
    $this->conn = \DB\Connection::sharedInstance();
  }

  public function signIn($email, $password) {
    $stm = $this->conn->prepare("SELECT * FROM users WHERE email = :email");
    $stm->bindParam(":email", $email);

    if($stm->execute()) {
      while($row = $stm->fetch()) {
        $this->setUserSession(\Model\User::retrieveWithEmail($row["email"]));
        return $row["password"] === md5($password);
      }
    }
    else {
      return false;
    }
  }

  private function setUserSession($user) {
    if(!isset($_SESSION["loggedUser"])) {
      $_SESSION["loggedUser"] = $user;
    }
  }
}

?>
