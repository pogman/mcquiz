<?php

namespace Model;

abstract class BaseModel {
  abstract protected function save();
  abstract protected static function retrieve($id);
  abstract protected static function delete($id);
}

?>
