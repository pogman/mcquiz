<?php 

namespace Model;

require 'db/connection.php';

class Option {
  public $id;
  public $value;

  public function __construct($value) {
    $this->value = $value;
  }

  public static function retrieveAllOptionsForQuestion($question_id) {
    $conn = \DB\Connection::sharedInstance();

    $stm = $conn->prepare("SELECT * FROM options INNER JOIN questions ON options.question_id = questions.id WHERE questions.id = :question_id");

    $stm->bindParam(":question_id", $question_id);
    $options = array();

    if($stm->execute()) {
      while($row = $stm->fetch()) {
        $option = new Option($row["value"]);
        $option->id = intval($row["id"]);

        $options[] = $option;
      }

      return $options;
    }
  }
}

?>
