<?php

namespace Model;

require "db/connection.php";
require "model/base_model.php";

class User extends BaseModel {
  public $id;
  public $name;
  public $email;
  public $password;

  public function __construct($name, $email, $password) {
    $this->name = $name;
    $this->email = $email;
    $this->password = $password;
  }

  public function save() {
    $conn = \DB\Connection::sharedInstance();
    
    $stm = $conn->prepare("INSERT INTO users (name, email, password) VALUES (:name, :email, :password)");

    $stm->bindParam(":name", $this->name);
    $stm->bindParam(":email", $this->email);

    $md5_password = md5($this->password);
    $stm->bindParam(":password", $md5_password);

    return $stm->execute();
  }

  public static function retrieve($id) {
    $conn = \DB\Connection::sharedInstance();
    
    $stm = $conn->prepare("SELECT * FROM users WHERE id = :id");
    $stm->bindParam(":id", $id);

    if($stm->execute()) {
      while($row = $stm->fetch()) {
        $user = new User($row["name"], $row["email"], $row["password"]);
        $user->id = intval($row["id"]);

        return $user;
      }
    }
  }

  public static function retrieveWithEmail($email) {
    $conn = \DB\Connection::sharedInstance();
    
    $stm = $conn->prepare("SELECT * FROM users WHERE email = :email");
    $stm->bindParam(":email", $email);

    if($stm->execute()) {
      while($row = $stm->fetch()) {
        $user = new User($row["name"], $row["email"], $row["password"]);
        $user->id = intval($row["id"]);

        return $user;
      }
    }
  }

  public static function delete($id) {
    $conn = \DB\Connection::sharedInstance();

    $stm = $conn->prepare("DELETE FROM users WHERE id = :id");
    $stm->bindParam(":id", $id);
    
    return $stm->execute();
  }
}

?>
