<?php 

namespace Model;

require "model/base_model.php";
require "model/option.php";

class Question extends BaseModel {
  public $id;
  public $description;
  public $answer;
  public $options;

  public function __construct($description, $answer) {
    $this->description = $description;
    $this->answer = $answer;
  }

  public function save() {
    $conn = \DB\Connection::sharedInstance();
    
    $stm = $conn->prepare("INSERT INTO questions (description, answer) VALUES (:description, :answer)");

    $stm->bindParam(":description", $this->description);
    $stm->bindParam(":answer", $this->answer);

    return $stm->execute();
  }

  public static function retrieve($id) {
    $conn = \DB\Connection::sharedInstance();
    
    $stm = $conn->prepare("SELECT * FROM questions WHERE id = :id");
    $stm->bindParam(":id", $id);

    if($stm->execute()) {
      while($row = $stm->fetch()) {
        $question = new Question($row["description"], $row["answer"]);
        $question->id = intval($row["id"]);

        $question->options = Option::retrieveAllOptionsForQuestion($question->id);

        return $question;
      }
    }
  }

  public static function delete($id) {
    $conn = \DB\Connection::sharedInstance();

    $stm = $conn->prepare("DELETE FROM questions WHERE id = :id");
    $stm->bindParam(":id", $id);
    
    return $stm->execute();
  }
}

?>
